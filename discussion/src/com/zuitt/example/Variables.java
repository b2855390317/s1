//package in Java is used to group related to classes
//think of it as a folder in a file directory
    //packages are divided into 2 categ.
    //1. Built-in Packages(JAVA API)
    //2.User-defined Packages (create your own package)

//reverse domain name notation
package com.zuitt.example;

public class Variables {
    public static void main(String[] args){

        //Naming conventions
        //the terminology used for variables names is "identifiers"
        //Syntax: dataType identifier;

        int age;
        char middleName;

        int x;
        int y = 1;

        //initialization after declaration
        x = 1;

        //change values
        y = 2;

        System.out.println("the value of y is " + y + " and the value of x " + x);

        //Primitive Data Types
        //predefined within the Java programming language which is used for single-valued var with limited capabilities.

        //int- whole number val

        int wholeNum = 100;

        System.out.println(wholeNum);

        //long
        //L is added to the end of the long numbers to be recognized

        long worldPopulation = 89999991412L;
        System.out.println(worldPopulation);

       //float
        //add f at the end of float
        float piFloat = 3.14159265359f;
        System.out.println(piFloat);

        //double - floating point val
        double piDouble =  3.14159265359;
        System.out.println(piDouble);

        //char - single characters
        char letter = 'c';
        System.out.println(letter);

        //boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        //constant
        //final - keyword
        //common practice - CAPITAL LETTERS

        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

       //non-primitive data types - can store multiple values
        //aka as reference data types - refer to instances or objects
        // do not directly store the value of a var, but rather remembers the reference to the var

        //String - stores a sequence or array of characters.

        String userName = "CardoD";
        System.out.println(userName);

        int stringLength = userName.length();
        System.out.println(stringLength);
    }
}
